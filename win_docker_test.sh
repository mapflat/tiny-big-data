#! /usr/bin/env bash

set -e

mkdir -p /mapflat_exercise_unix
rsync -aP --delete --exclude out --exclude build --cvs-exclude ./* /mapflat_exercise_unix/
cd /mapflat_exercise_unix/
for f in $(find . -type f); do
  if file $f | grep -q 'with CRLF line'; then
    fromdos $f
  fi
done
./gradlew test
