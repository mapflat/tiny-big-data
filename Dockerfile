FROM ubuntu:16.04

RUN apt-get update && apt-get install -y python3-pip && pip3 install stuffer

RUN stuffer 'contrib.java.Jdk(8)'
RUN stuffer 'apt.Install("scala")'
RUN stuffer 'apt.Install("git")'
RUN stuffer 'apt.Install("tofrodos")'

RUN mkdir /mapflat_exercise
WORKDIR /mapflat_exercise
