package com.mapflat.tinybigdata

import better.files.File
import org.scalatest.FunSuite
import play.api.libs.json.{Json, Reads}

case class TestOrder(user: String, item: Int)

class CollTest extends FunSuite {
  test("construct empty coll") {
    assert(Coll.empty[Int]().collect() === List.empty[Int])
  }

  test("construct from vector") {
    assert(Coll.from(Vector(1)).collect() === List(1))
  }

  test("cross join") {
    assert(Coll.from(Vector(1, 2)).cross(Coll.from(Vector(3, 4, 5))).collect().sorted ===
      List(
        (1, 3), (1, 4), (1, 5),
        (2, 3), (2, 4), (2, 5)))
  }

  test("filter") {
    assert(Coll.from(Vector(1, 2, 3, 4, 5)).filter(_ % 2 == 0).collect() === List(2, 4))
  }

  test("flatMap") {
    assert(Coll.from(Vector(2, 4)).flatMap(i => 0 until i).collect() === List(0, 1, 0, 1, 2, 3))
  }

  test("foreach") {
    var sum = 0
    Coll.from(Vector(2, 4)).foreach(i => sum += i)
    assert(sum === 6)
  }

  test("groupAll") {
    assert(Coll.from(List(2, 3, 5)).groupAll.collect() === List(((), 2), ((), 3), ((), 5)))
  }

  test("groupBy") {
    assert(Coll.from(List(2, 3, 5)).groupBy(_ % 2 == 0).collect() === List((true, 2), (false, 3), (false, 5)))
  }

  test("log") {
    val logOutput = new StringBuilder
    Coll.from(Vector(1, 3, 4)).log("Test log", { t: String => logOutput ++= t + "\n" })
    assert(logOutput.toString === "Test log: 1\nTest log: 3\nTest log: 4\n\n")
  }

  test("map") {
    assert(Coll.from(Vector(3, 5)).map(_ + 3).collect() === List(6, 8))
  }

  test("union") {
    assert(Coll.from(List(1)).union(Coll.from(List(2, 3))).collect() === List(1, 2, 3))
  }

  test("toString") {
    assert(Coll.from(Vector(3, 5)).toString === "[\n  3,\n  5\n]")
  }

  test("readJson empty file") {
    implicit val orderReads: Reads[TestOrder] = Json.reads[TestOrder]

    for {
      jsonFile <- File.temporaryFile()
    } {
      jsonFile.write("\n")
      assert(Coll.readJson[TestOrder](jsonFile.path.toString).collect() === List())
    }
  }

  test("readJson from file") {
    implicit val orderReads: Reads[TestOrder] = Json.reads[TestOrder]
    implicit val orderOrdering: Ordering[TestOrder] = Ordering.by(TestOrder.unapply)

    for {
      jsonFile <- File.temporaryFile()
    } {
      jsonFile.write("""{"user": "alice", "item": 1}""" + "\n" + """{"user": "bob", "item": 2}""" + "\n")
      assert(Coll.readJson[TestOrder](jsonFile.path.toString).collect().sorted ===
        List(TestOrder("alice", 1), TestOrder("bob", 2)))
    }
  }

  test("readJson from directory") {
    implicit val orderReads: Reads[TestOrder] = Json.reads[TestOrder]
    implicit val orderOrdering: Ordering[TestOrder] = Ordering.by(TestOrder.unapply)

    for {
      jsonDir <- File.temporaryDirectory()
    } {
      (jsonDir / "part-00001.json").write(
        """{"user": "alice", "item": 1}""" + "\n" + """{"user": "bob", "item": 2}""" + "\n")
      (jsonDir / "part-00004.json").write(
        """{"user": "curt", "item": 3}""" + "\n")

      assert(Coll.readJson[TestOrder](jsonDir.path.toString).collect().sorted ===
        List(TestOrder("alice", 1), TestOrder("bob", 2), TestOrder("curt", 3)))
    }
  }
}
