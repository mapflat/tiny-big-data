package com.mapflat.exercises.intermediate

import better.files.File
import com.github.nscala_time.time.Imports._
import com.mapflat.exercises.{BatchJobTestRunner1, PageView, Serde, Session}
import org.scalatest.FunSuite


class UserSessionsJobTest extends FunSuite with BatchJobTestRunner1[PageView, Session] {

  import Serde._

  implicit val sessionOrdering: Ordering[Session] = Ordering.by({
    session => (session.start.getMillis, session.end.getMillis, session.user)
  })

  override def runJob(pageView: File, output: File): Unit =
    UserSessionsJob.main(Array("UserSessionsJob", pageView.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

  def ten: DateTime = DateTime.parse("2018-01-01T10:00:00Z")
  val url = "http://www.acme.com/about.html"
  
  test("One page view") {
    assert(runTest(Seq(PageView("alice", url,  ten))) ===
      Seq(Session("alice", ten, ten)))
  }

  test("Two page views") {
    assert(runTest(Seq(
      PageView("alice", url,  ten),
      PageView("alice", url,  ten + 1.minute)
    )) ===
      Seq(Session("alice", ten, ten + 1.minute)))
  }

  test("Two page views same time") {
    assert(runTest(Seq(
      PageView("alice", url,  ten),
      PageView("alice", url,  ten)
    )) ===
      Seq(Session("alice", ten, ten)))
  }

  test("Two users") {
    assert(runTest(Seq(
      PageView("alice", url,  ten),
      PageView("bob", url,  ten + 1.minutes),
      PageView("alice", url,  ten + 2.minutes),
      PageView("bob", url,  ten + 3.minutes)
    )) ===
      Seq(
        Session("alice", ten, ten + 2.minutes),
        Session("bob", ten + 1.minutes, ten + 3.minutes)
      ))
  }

  test("Two sessions same user") {
    assert(runTest(Seq(
      PageView("alice", url,  ten),
      PageView("alice", url,  ten + 1.minutes),
      PageView("alice", url,  ten + 7.minutes),
      PageView("alice", url,  ten + 8.minutes)
    )) ===
      Seq(
        Session("alice", ten, ten + 1.minutes),
        Session("alice", ten + 7.minutes, ten + 8.minutes)
      ))
  }

  test("Too long session") {
    assert(runTest((0 until 190 by 4).map(offset => PageView("alice", url,  ten + offset.minutes))) ===
      Seq(
        Session("alice", ten, ten + 180.minutes),
        Session("alice", ten + 184.minutes, ten + 188.minutes)
      ))
  }
}
