package com.mapflat.exercises.intermediate

import better.files.File
import com.github.nscala_time.time.Imports._
import com.mapflat.exercises._
import org.joda.time.DateTime
import org.joda.time.DateTimeZone.UTC
import org.scalatest.FunSuite

class InactiveUsersJobTest extends FunSuite with BatchJobTestRunner2[PageView, User, UserLastActive] {

  import Serde._

  implicit val userLastActiveOrdering: Ordering[UserLastActive] = Ordering.by(UserLastActive.unapply)

  override def runJob(pageView: File, user: File, output: File): Unit =
    InactiveUsersJob.main(Array("ViewTimeDeclineJob", "2018-03-28", "7",
      pageView.toString, user.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq(), Seq()) === Seq())
  }

  val url = "http://www.acme.com/"
  val today = new DateTime(2018, 3, 28, 0, 0, UTC)
  val millenial = new DateTime(2000, 1, 1, 0, 0, UTC)

  test("One inactive") {
    assert(runTest(Seq(
      PageView("alice", url, today - 9.days)),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq(UserLastActive("alice", "Alice Springs", today - 9.days))
    )
  }

  test("One active") {
    assert(runTest(Seq(
      PageView("alice", url, today - 6.days)),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq()
    )
  }

  test("One foreign inactive") {
    assert(runTest(Seq(
      PageView("alice ", url, today - 9.days)),
      Seq(
        User("alice", "Alice Springs", "AU", millenial, "female"))) ===
      Seq()
    )
  }

  test("One inactive with trailing whitespace") {
    assert(runTest(Seq(
      PageView("alice ", url, today - 9.days)),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq(UserLastActive("alice", "Alice Springs", today - 9.days))
    )
  }

  test("One inactive, one old + recent action, one no action") {
    assert(runTest(Seq(
      PageView("alice ", url, today - 9.days),
      PageView("bob", url, today - 10.days),
      PageView("bob  ", url, today - 1.day)),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("bob", "Bob Hope", "SE", millenial + 1.year, "male"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female"))) ===
      Seq(UserLastActive("alice", "Alice Springs", today - 9.days))
    )
  }

  test("One two old actions, one old + recent action, one non-Swede, one no action") {
    assert(runTest(Seq(
      PageView("alice ", url, today - 11.days),
      PageView("alice ", url, today - 9.days),
      PageView("david", url, today - 10.days),
      PageView("bob  ", url, today - 1.day),
      PageView("david  ", url, today - 1.day)),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("bob", "Bob Hope", "UK", millenial + 1.year, "male"),
        User("david", "David Beckham", "SE", millenial + 1.year, "male"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female"))) ===
      Seq(UserLastActive("alice", "Alice Springs", today - 9.days))
    )
  }
}
