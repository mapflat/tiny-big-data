package com.mapflat.exercises

import better.files.File
import com.mapflat.tinybigdata.Coll.readJson
import play.api.libs.json.{Json, Reads, Writes}

trait BatchJobTestRunner {

  def writeInput[T](tmpDir: File, index: Int, input: Seq[T])(implicit inWrites: Writes[T]): File = {
    val inputDir = tmpDir / f"input$index"
    inputDir.createDirectory()
    val inputFile = inputDir / "part-00001.json"
    inputFile.write(input.map(item => Json.stringify(Json.toJson(item))).mkString("", "\n", "\n"))
    println(s"Created input file ${inputFile.path}")
    inputDir
  }

  def readOutput[U](outputFile: File)(implicit outReads: Reads[U], outOrdering: Ordering[U]): Seq[U] = {
    if (outputFile.isDirectory)
      outputFile.glob("*.json").map(f => readOutput[U](f)).reduce((a, b) => a.union(b)).sorted
    else {
      println(s"Reading job output file ${outputFile.path}")
      val lines = outputFile.lines.toSeq.filter(line => !line.trim.isEmpty)
      val output = lines.map(line => Json.fromJson[U](Json.parse(line)).get)
      output.sorted
    }
  }
}

trait BatchJobTestRunner1[T, U] extends BatchJobTestRunner {
  def runJob(inputPath: File, outputPath: File): Unit

  def runTest(input: Seq[T])(implicit inReads: Reads[T], inWrites: Writes[T],
                             outReads: Reads[U], outWrites: Writes[U],
                             outOrdering: Ordering[U]): Seq[U] = {
    File.temporaryDirectory()(
      tmpDir => {
        val inputDir = writeInput(tmpDir, 1, input)

        val outputDir = tmpDir / "output"
        runJob(inputDir.path, outputDir.path)
        readOutput[U](outputDir)
      })
  }
}

trait BatchJobTestRunner2[T1, T2, U] extends BatchJobTestRunner {
  def runJob(inputPath1: File, inputPath2: File, outputPath: File): Unit

  def runTest(input1: Seq[T1], input2: Seq[T2])(
    implicit
    inReads1: Reads[T1], inWrites1: Writes[T1],
    inReads2: Reads[T2], inWrites2: Writes[T2],
    outReads: Reads[U], outWrites: Writes[U],
    outOrdering: Ordering[U]): Seq[U] = {
    File.temporaryDirectory()(
      tmpDir => {
        val inputDir1 = writeInput(tmpDir, 1, input1)
        val inputDir2 = writeInput(tmpDir, 2, input2)
        val outputDir = tmpDir / "output"
        runJob(inputDir1.path, inputDir2.path, outputDir.path)
        readOutput[U](outputDir)
      })
  }
}
