package com.mapflat.exercises

import java.util.UUID

object TestInput {
  def orders(count: Int, user: String, item: String): Seq[Order] =
    Seq.fill(count) {Order(UUID.randomUUID().toString, user, item)}
}
