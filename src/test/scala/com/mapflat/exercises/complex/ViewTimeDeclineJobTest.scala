package com.mapflat.exercises.complex

import better.files.File
import com.github.nscala_time.time.Imports.{DateTime, _}
import com.mapflat.exercises._
import org.scalatest.FunSuite

import scala.Ordering.Implicits._

class ViewTimeDeclineJobTest extends FunSuite with BatchJobTestRunner1[PageView, ViewTimeDecline] {

  import Serde._

  implicit val viewTimePercentOrdering: Ordering[ViewTimePercent] = Ordering.by(ViewTimePercent.unapply)

  implicit val viewTimeDeclineOrdering: Ordering[ViewTimeDecline] = Ordering.by(
    decline => (decline.url, decline.viewCount, decline.viewTimePercentages.sorted)
  )

  override def runJob(pageView: File, output: File): Unit =
    ViewTimeDeclineJob.main(Array("ViewTimeDeclineJob", pageView.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

  def ten: DateTime = DateTime.parse("2018-01-01T10:00:00Z")

  val url = "http://www.acme.com/"

  test("One page view") {
    assert(runTest(Seq(PageView("alice", url, ten))) === Seq())
  }

  test("One view time") {
    assert(runTest(Seq(
      PageView("alice", url, ten),
      PageView("alice", url + "/about.html", ten + 61.seconds)
    )) ===
      Seq(ViewTimeDecline(url, 1,
        ((0 to 60 by 10).map(sec => ViewTimePercent(sec, 100)) ++
          (70 to 170 by 10).map(sec => ViewTimePercent(sec, 0))).toList)))
  }

  test("Two view times, one user") {
    assert(runTest(Seq(
      PageView("alice", url, ten),
      PageView("alice", url + "/about.html", ten + 61.seconds),
      PageView("alice", url, ten + 5.minutes),
      PageView("alice", url + "/contact.html", ten + 5.minutes + 23.seconds)
    )) ===
      Seq(ViewTimeDecline(url, 2,
        ((0 to 20 by 10).map(sec => ViewTimePercent(sec, 100)) ++
          (30 to 60 by 10).map(sec => ViewTimePercent(sec, 50)) ++
          (70 to 170 by 10).map(sec => ViewTimePercent(sec, 0))).toList)))
  }

  test("Two view times, different users") {
    assert(runTest(Seq(
      PageView("alice", url, ten),
      PageView("alice", url + "/about.html", ten + 61.seconds),
      PageView("bob", url, ten + 1.minute),
      PageView("bob", url + "/contact.html", ten + 1.minute + 23.seconds)
    )) ===
      Seq(ViewTimeDecline(url, 2,
        ((0 to 20 by 10).map(sec => ViewTimePercent(sec, 100)) ++
          (30 to 60 by 10).map(sec => ViewTimePercent(sec, 50)) ++
          (70 to 170 by 10).map(sec => ViewTimePercent(sec, 0))).toList)))
  }

  test("Four view times") {
    assert(runTest(Seq(
      PageView("alice", url, ten),
      PageView("alice", url, ten + 61.seconds),
      PageView("alice", url, ten + 182.seconds),
      PageView("alice", url, ten + 191.seconds),
      PageView("alice", url, ten + 362.seconds)
    )) ===
      Seq(ViewTimeDecline(url, 4,
        ((0 to 0 by 10).map(sec => ViewTimePercent(sec, 100)) ++
          (10 to 60 by 10).map(sec => ViewTimePercent(sec, 75)) ++
          (70 to 120 by 10).map(sec => ViewTimePercent(sec, 50)) ++
          (130 to 170 by 10).map(sec => ViewTimePercent(sec, 25))).toList)))
  }

  test("Two urls") {
    assert(runTest(Seq(
      PageView("alice", url, ten),
      PageView("alice", url + "/about.html", ten + 61.seconds),
      PageView("alice", url, ten + 3.minutes),
      PageView("alice", url + "/contact.html", ten + 5.minutes + 23.seconds)
    )) ===
      Seq(ViewTimeDecline(url, 2,
        ((0 to 60 by 10).map(sec => ViewTimePercent(sec, 100)) ++
          (70 to 140 by 10).map(sec => ViewTimePercent(sec, 50)) ++
          (150 to 170 by 10).map(sec => ViewTimePercent(sec, 0))).toList),
        ViewTimeDecline(url + "/about.html", 1,
        ((0 to 110 by 10).map(sec => ViewTimePercent(sec, 100)) ++
          (120 to 170 by 10).map(sec => ViewTimePercent(sec, 0))).toList)))
  }
}
