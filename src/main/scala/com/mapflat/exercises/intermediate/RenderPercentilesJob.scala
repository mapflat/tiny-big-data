package com.mapflat.exercises.intermediate

import com.mapflat.exercises._
import com.mapflat.tinybigdata.Coll


// Given a dataset with page rendering events, containing web page URLs and render times, calculate the
// 1%, 5%, 10%, 50%, 90%, 95%, 99% percentiles for rendering times per URL.

object RenderPercentilesJob {

  // Helper function: From a sorted vector of values, get the value at the nth percentile.
  def percentile(percent: Int, sortedMillis: Vector[Int]): Int = {
    sortedMillis((percent / 100.0 * sortedMillis.size).toInt)
  }

  def main(args: Array[String]): Unit = {
    import Serde._

    val pageRenderPath = args(1)
    val outputPath = args(2)

    val pageRender = Coll.readJson[PageRender](pageRenderPath)

    // solution_begin
    val result: Coll[RenderPercentiles] = pageRender
      .groupBy(_.url)
      .fold(Vector.empty[Int])((agg, element) => agg :+ element.renderTimeMillis)
      .map(tup => buildPercentiles(tup._1, tup._2))

    result.writeJson(outputPath)
  }

  private def buildPercentiles(url: String, renderMillis: Vector[Int]) = {
    val sortedMillis = renderMillis.sorted
    RenderPercentiles(url,
      percentile(1, sortedMillis),
      percentile(5, sortedMillis),
      percentile(10, sortedMillis),
      percentile(50, sortedMillis),
      percentile(90, sortedMillis),
      percentile(95, sortedMillis),
      percentile(99, sortedMillis))

    // solution_end
    // exercise: val result: Coll[RenderPercentiles] = Coll.empty[RenderPercentiles]
    // exercise: result.writeJson(outputPath)
  }
}
