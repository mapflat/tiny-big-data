package com.mapflat.exercises.intermediate

import com.github.nscala_time.time.Imports._
import com.mapflat.exercises.Serde._
import com.mapflat.exercises._
import com.mapflat.tinybigdata.Coll
import org.joda.time.PeriodType
import org.joda.time.format.ISODateTimeFormat

// Read datasets with PageViews and User data. Emit a list of Swedish users that have not been active in n days,
// in order to send them nag mail. Due to a faulty client library, some PageView events have trailing white space
// in the user field. Users without any activity at all should not get a mail.
//
// The current date and the age limit in days are passed as command line arguments to the job.

object InactiveUsersJob {
  def main(args: Array[String]): Unit = {
    val date = ISODateTimeFormat.date().parseDateTime(args(1))
    val ageDays = args(2).toInt
    val pageViewPath = args(3)
    val userPath = args(4)
    val outputPath = args(5)

    val pageView = Coll.readJson[PageView](pageViewPath)
    val user = Coll.readJson[User](userPath)

    def isOld(pv: PageView): Boolean = {
      val interval: Interval = pv.time to date
      val period: Period = interval.toPeriod(PeriodType.days())
      val days: Int = period.days
      days > ageDays
    }

    // solution_begin
    val pageViewMended = pageView
      .log("input")
      .map(pv => pv.copy(user = pv.user.trim))
      .log("mended")

    val lastPageView = pageViewMended
      .groupBy(_.user)
      .reduce((left, right) => if (left.time > right.time) left else right)
      .log("last page view")

    val inactive = lastPageView
      .values
      .filter(isOld)
      .log("Only inactive")

    val swedes = user
      .filter(_.country == "SE")

    val lastWithUser: Coll[(PageView, User)] = inactive
      .groupBy(_.user)
      // Since we join with Swedish users only, the other page views will get dropped.
      .join(swedes.groupBy(_.id))
      .values
      .log("With users")

    val result: Coll[UserLastActive] = lastWithUser
      .map({ case (pv, u) => UserLastActive(u.id, u.name, pv.time) })
    // solution_end
    // exercise: val result: Coll[UserLastActive] = Coll.empty[UserLastActive]

    result.writeJson(outputPath)
  }
}


