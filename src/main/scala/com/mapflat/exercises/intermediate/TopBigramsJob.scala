package com.mapflat.exercises.intermediate

import com.mapflat.exercises.{BigramCount, MessagePost}
import com.mapflat.tinybigdata.Coll
import play.api.libs.json._

import scala.annotation.tailrec

// A bigram is a pair of words next to each other in a text. Read datasets with messages (e.g. posted to a forum) and
// emit a dataset with the n most popular bigrams, with counts.

object TopBigramsJob {
  def main(args: Array[String]): Unit = {
    implicit val postReads: Reads[MessagePost] = Json.reads[MessagePost]
    implicit val bigramWrites: Writes[BigramCount] = Json.writes[BigramCount]

    val topCount = args(1).toInt
    val inputFile = args(2)
    val outputFile = args(3)

    val posts = Coll.readJson[MessagePost](inputFile)

    // solution_begin
    val words: Coll[List[String]] = posts
      .map(_.content.replaceAll("[^A-Za-z0-9_-]", " "))
      .map(_.split("\\s+").toList)

    val bigrams: Coll[(String, String)] = words
      .flatMap(window)

    val bigramCounts: Coll[BigramCount] = bigrams
      .groupBy(identity)
      .count
      .map(tup => BigramCount(tup._1._1, tup._1._2, tup._2))

    val result: Coll[BigramCount] = bigramCounts
      .groupAll
      .sortBy(bc => -bc.count)
      .take(topCount)
      .values

    result.writeJson(outputFile)
  }

  def window(words: List[String]): List[(String, String)] =
    windowImpl(words, List())

  // Accumulate the result in a parameter in order to enable tail recursion.
  @tailrec
  private def windowImpl(words: List[String], result: List[(String, String)]): List[(String, String)] =
    if (words.lengthCompare(2) < 0)
      result
    else
      windowImpl(words.tail, result :+ (words.head, words(1)))

  // solution_end
  // exercise:   val result: Coll[BigramCount] = Coll.empty[BigramCount]
  // exercise:   result.writeJson(outputFile)
  // exercise: }
}
