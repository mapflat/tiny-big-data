package com.mapflat.exercises.intermediate

import com.github.nscala_time.time.Imports._
import com.mapflat.exercises.Serde._
import com.mapflat.exercises.{PageView, Session}
import com.mapflat.tinybigdata.Coll

// A web session is a sequence of page views from one user at most five minutes apart. A session can be no longer than
// three hours. Given a dataset of page views, emit a dataset with sessions.

object UserSessionsJob {
  def main(args: Array[String]): Unit = {
    val pageViewPath = args(1)
    val outputPath = args(2)

    val pageViews = Coll.readJson[PageView](pageViewPath)

    // solution_begin

    def isSameSession(session: Session, pageView: PageView) =
      pageView.time >= session.start &&
        pageView.time <= session.end + 5.minutes &&
        pageView.time <= session.start + 3.hours

    def addToSessions(sessions: List[Session], pageView: PageView): List[Session] =
      if (sessions.isEmpty)
        List(Session(pageView.user, pageView.time, pageView.time))
      else if (isSameSession(sessions.last, pageView))
        sessions.dropRight(1) :+ Session(pageView.user, sessions.last.start, pageView.time)
      else
        sessions :+ Session(pageView.user, pageView.time, pageView.time)

    val sorted = pageViews
      .groupBy(_.user)
      .sortBy(_.time)

    val result: Coll[Session] = sorted
      .fold(List[Session]())(addToSessions)
      .values
      .flatMap(t => t)

    // solution_end
    // exercise: val result: Coll[Session] = Coll.empty[Session]
    result.writeJson(outputPath)
  }

}
