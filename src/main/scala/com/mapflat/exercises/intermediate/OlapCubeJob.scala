package com.mapflat.exercises.intermediate

import com.github.nscala_time.time.Imports._
import com.mapflat.exercises.Serde._
import com.mapflat.exercises.{Order, OrderAggregate, User}
import com.mapflat.tinybigdata.Coll
import org.joda.time.PeriodType
import org.joda.time.format.ISODateTimeFormat

// Read a dataset with orders. Join with users and aggregate counts on these dimensions: country, age, gender, item.

object OlapCubeJob {
  def main(args: Array[String]): Unit = {
    val date = ISODateTimeFormat.date().parseDateTime(args(1))
    val orderPath = args(2)
    val userPath = args(3)
    val outputPath = args(4)

    val order = Coll.readJson[Order](orderPath)
    val user = Coll.readJson[User](userPath)

    def calculateAge(birthDate: DateTime): Int = (birthDate to date).toPeriod(PeriodType.years()).years

    // solution_begin
    val orderDemo = order.groupBy(_.user)
      .join(user.groupBy(_.id))
      .values
      .map(orderUser => {
        val (order, user) = orderUser
        val age = calculateAge(user.birthDate)
        OrderDemographical(order.id, order.item, user.country, age, user.gender)
      })

    val result: Coll[OrderAggregate] = orderDemo
      .groupBy(od => (od.item, od.country, od.age, od.gender))
      .count
      .map(kv => OrderAggregate(kv._1._1, kv._1._2, kv._1._3, kv._1._4, kv._2))
    // solution_end
    // exercise: val result: Coll[OrderAggregate] = Coll.empty[OrderAggregate]

    result.writeJson(outputPath)
  }
}

// Internal useful utility class. An order decorated with demographic information.
case class OrderDemographical(id: String, item: String, country: String, age: Int, gender: String)

