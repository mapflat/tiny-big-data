package com.mapflat.exercises.intermediate

import com.mapflat.exercises.{ItemDelta, Order, Serde, TrendingItem}
import com.mapflat.tinybigdata.{Coll, KColl}


// Given a dataset with today's orders and one with yesterday's orders, emit a top list with the n items that have the
// highest relative increase in volume. The top list output is unordered, since it is a dataset, but each record should
// have a field denoting the position in the top list. The increase, rounded to whole percents, should also be included.

object TopTrendingJob {

  def main(args: Array[String]): Unit = {
    import Serde._

    val topCount = args(1).toInt
    val yesterdayPath = args(2)
    val todayPath = args(3)
    val outputPath = args(4)

    val yesterday = Coll.readJson[Order](yesterdayPath)
    val today = Coll.readJson[Order](todayPath)

    // solution_begin
    val yesterdayCounts: KColl[String, Long] = yesterday
      .groupBy(_.item)
      .count

    val todayCounts: KColl[String, Long] = today
      .groupBy(_.item)
      .count

    val increase: Coll[ItemDelta] =
      yesterdayCounts
        .leftJoin(todayCounts) // Left join. Drop items that were not present yesterday.
        .map(tup => ItemDelta(item = tup._1, previousCount = tup._2._1, newCount = tup._2._2.getOrElse(0)))

    val topTrending: List[ItemDelta] = increase
      .groupAll
      .sortBy(delta => -delta.increase)
      .take(topCount)
      .values
      .collect()

    val topList: List[TrendingItem] = topTrending
      .zip(1 to topCount)
      .map(tup => TrendingItem(item = tup._1.item, position = tup._2, increasePercent = (tup._1.increase * 100).toInt))

    val result: Coll[TrendingItem] = Coll.from(topList)
    // solution_end
    // exercise: val result: Coll[TrendingItem] = Coll.empty[TrendingItem]
    result.writeJson(outputPath)
  }
}
