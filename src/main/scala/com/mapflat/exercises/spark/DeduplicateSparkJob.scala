package com.mapflat.exercises.spark

import com.mapflat.exercises.Order
import org.apache.spark.sql.{Dataset, SparkSession}

// Read a dataset with orders. Some orders are duplicates, and have identical id fields. Remove duplicates and ensure
// that each order has a unique id.

object DeduplicateSparkJob {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[1]").getOrCreate()
    run(args, spark)
  }

  def run(args: Array[String], spark: SparkSession): Unit = {
    import spark.implicits._

    val inputFile = args(1)
    val outputFile = args(2)

    val input: Dataset[Order] = spark.read.json(inputFile).as[Order]

    // solution_begin
    val result: Dataset[Order] = input
      .groupByKey(_.id)
      .reduceGroups((a, b) => a)
      .map(_._2)
    // solution_end
    // exercise: val result: Dataset[Order] = ???
    result.write.json(outputFile)
  }
}
