package com.mapflat.exercises.simple

import com.mapflat.exercises.Order
import com.mapflat.exercises.Serde._
import com.mapflat.tinybigdata.Coll

// Read a dataset with orders. Some orders have invalid fields. Drop the orders whose id or item fields are empty
// strings. For orders with empty strings as users, replace with <unknown_user>.

object WashJob {
  def main(args: Array[String]): Unit = {
    val inputFile = args(1)
    val outputFile = args(2)

    val input = Coll.readJson[Order](inputFile)
    // solution_begin
    val result: Coll[Order] = input
      .filter(isValid)
      .map(repair)
    result.writeJson(outputFile)
  }

  private def isValid(order: Order) = order.id != "" && order.item != ""

  private def repair(order: Order): Order = order match {
    case Order(id, "", item) => order.copy(user = "<unknown_user>")
    case o => o

    // solution_end
    // exercise: val result: Coll[Order] = Coll.empty[Order]
    // exercise: result.writeJson(outputFile)
  }
}
