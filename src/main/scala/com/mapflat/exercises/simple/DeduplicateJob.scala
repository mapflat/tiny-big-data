package com.mapflat.exercises.simple

import com.mapflat.exercises.Order
import com.mapflat.exercises.Serde._
import com.mapflat.tinybigdata.Coll

// Read a dataset with orders. Some orders are duplicates, and have identical id fields. Remove duplicates and ensure
// that each order has a unique id.

object DeduplicateJob {
  def main(args: Array[String]): Unit = {
    val inputFile = args(1)
    val outputFile = args(2)

    val input = Coll.readJson[Order](inputFile)

    // solution_begin
    val result: Coll[Order] = input
      .groupBy(_.id)
      .reduce((a, b) => a)
      .values
    // solution_end
    // exercise: val result: Coll[Order] = Coll.empty[Order]
    result.writeJson(outputFile)
  }
}
