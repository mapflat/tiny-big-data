package com.mapflat.exercises.simple

import com.mapflat.exercises.{ItemCount, Order, Serde}
import com.mapflat.tinybigdata.Coll

// Given a dataset of orders, emit a dataset with the n most popular items, with counts.


object TopItemsJob {
  def main(args: Array[String]): Unit = {
    import Serde._

    val topCount = args(1).toInt
    val inputFile = args(2)
    val outputFile = args(3)

    val orders = Coll.readJson[Order](inputFile)

    // solution_begin

    val itemCounts: Coll[ItemCount] = orders
      .groupBy(_.item)
      .count
      .map(tup => ItemCount(tup._1, tup._2))

    val result: Coll[ItemCount] = itemCounts
      .groupAll
      .sortBy(bc => -bc.count)
      .take(topCount)
      .values
    // solution_end
    // exercise: val result: Coll[ItemCount] = Coll.empty[ItemCount]

    result.writeJson(outputFile)
  }
}
