package com.mapflat.exercises.simple

import com.mapflat.exercises.Serde._
import com.mapflat.exercises._
import com.mapflat.tinybigdata.Coll

// Read a dataset with orders. Join with users and count the number of orders per country.

object CountryCountJob {
  def main(args: Array[String]): Unit = {
    val orderPath = args(1)
    val userPath = args(2)
    val outputPath = args(3)

    val order = Coll.readJson[Order](orderPath)
    val user = Coll.readJson[User](userPath)

    // solution_begin

    val orderUser = order.groupBy(_.user)
      .join(user.groupBy(_.id))
      .values

    val result: Coll[CountryCount] = orderUser
      .groupBy(_._2.country)
      .count
      .map(tup => CountryCount(tup._1, tup._2))

    // solution_end
    // exercise: val result: Coll[CountryCount] = Coll.empty[CountryCount]
    result.writeJson(outputPath)
  }
}
