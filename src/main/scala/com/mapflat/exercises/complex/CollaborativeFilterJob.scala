package com.mapflat.exercises.complex

import com.mapflat.exercises._
import com.mapflat.tinybigdata.Coll


// Read a dataset with page (movie) view events, containing web page URLs, user, and time stamps. For each user, figure
// out the user with most similar taste. For each user, lind a movie that the most similar user has viewed, but the
// user has not. Emit one recommendation for each user. Hint: use cosine similarity or Jaccard similarity for
// comparing user taste.

object CollaborativeFilterJob {

  def main(args: Array[String]): Unit = {
    import Serde._

    val pageViewPath = args(1)
    val outputPath = args(2)

    val pageView = Coll.readJson[PageView](pageViewPath)

    // solution_begin

    val userHistory = pageView
      .groupBy(_.user)
      .foldWithKey(user => ViewHistory(user, Map.empty))(
        (hist, view) => hist.increase(view.url))
      .values

    val userByUser: Coll[TasteSimilarity] = userHistory
      .cross(userHistory)
      .map(tup => TasteSimilarity(tup._1, tup._2))
      .filter(!_.sameUser)

    val nearestNeighbour: Coll[TasteSimilarity] = userByUser
      .groupBy(_.profile.user)
      .reduce((left, right) => if (left.cosineSimilarity > right.cosineSimilarity) left else right)
      .values

    val result = nearestNeighbour
      .flatMap(_.recommendation)

    // solution_end
    // exercise: val result: Coll[Recommendation] = Coll.empty[Recommendation]
    result.writeJson(outputPath)
  }
}

case class ViewHistory(user: String, viewCounts: Map[String, Long]) {
  def increase(url: String): ViewHistory = {
    val oldVal: Long = viewCounts.getOrElse(url, 0)
    copy(viewCounts = viewCounts.updated(url, oldVal + 1L))
  }
}


case class TasteSimilarity(profile: ViewHistory, other: ViewHistory) {
  def sameUser: Boolean = profile.user == other.user

  def cosineSimilarity: Double =
    dotProduct / (magnitude(profile) * magnitude(other))

  private def dotProduct = {
    val urls = profile.viewCounts.keySet ++ other.viewCounts.keySet // Don't actually need both, since we do a dot product.
    urls.map(url => profile.viewCounts.getOrElse(url, 0L) * other.viewCounts.getOrElse(url, 0L)).sum
  }

  private def magnitude(history: ViewHistory) =
    Math.sqrt(history.viewCounts.values.map(v => v * v).sum)

  def recommendation: Option[Recommendation] = {
    val notSeen = other.viewCounts.keySet -- profile.viewCounts.keySet
    // Pick an arbitrary not seen url from neighbour.
    for {
      url <- notSeen.headOption
    } yield Recommendation(profile.user, url)
  }
}
