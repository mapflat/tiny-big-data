package com.mapflat.exercises.complex

import com.github.nscala_time.time.Imports._
import com.mapflat.exercises._
import com.mapflat.tinybigdata.Coll
import org.joda.time.Duration


// Read a dataset with page view events, containing web page URLs, user, and time stamps. Determine the view time
// for each page view by comparing with the next view by the same user, capped to three minutes. For each url,
// emit the fraction of users still viewing a page after n seconds, with n ranging from 10 seconds to 170 seconds by
// 10 second increases..

object ViewTimeDeclineJob {

  // solution_begin
  case class ViewTimeBucket(url: String, time: Duration)

  case class BucketCount(bucket: ViewTimeBucket, count: Long)

  // solution_end

  def main(args: Array[String]): Unit = {
    import Serde._

    val pageViewPath = args(1)
    val outputPath = args(2)

    val pageView = Coll.readJson[PageView](pageViewPath)

    // solution_begin
    val viewTimes: Coll[ViewTimeBucket] = pageView
      .groupBy(_.user)
      .sortBy(_.time)
      .fold(List.empty[PageView])((agg, pageView) => agg :+ pageView)
      .values
      .flatMap(deltas)
      .filter(_.time <= 3.minutes)

    val timeBuckets: Coll[BucketCount] = viewTimes
      .groupBy(identity)
      .count
      .map(tup => BucketCount(tup._1, tup._2))
      .log("timeBuckets")

    val result: Coll[ViewTimeDecline] = timeBuckets
      .groupBy(_.bucket.url)
      .fold(List.empty[BucketCount])((agg, bc) => agg :+ bc)
      .values
      .map(decline)

    result.writeJson(outputPath)
  }

  private def timeBucket(first: PageView, second: PageView) =
    ViewTimeBucket(first.url, new Duration(((second.time.getMillis - first.time.getMillis) / 10000) * 10000))

  private def deltas(views: List[PageView]): List[ViewTimeBucket] = views match {
    case Nil => List.empty[ViewTimeBucket]
    case first :: Nil => List.empty[ViewTimeBucket]
    case first :: second :: rest => timeBucket(first, second) :: deltas(second :: rest)
  }

  private def decline(buckets: List[BucketCount]) = {
    val total = buckets.map(_.count).sum

    val cumulativeCounts = for {
      secs <- 0 to 170 by 10
      counts = buckets.filter(_.bucket.time >= secs.seconds).map(_.count)
    } yield secs -> counts.sum

    val fractions = for {
      cc <- cumulativeCounts
    } yield ViewTimePercent(cc._1, (cc._2 * 100 / total).toInt)

    ViewTimeDecline(buckets.head.bucket.url, total, fractions.toList)

    // solution_end
    // exercise: val result: Coll[ViewTimeDecline] = Coll.empty[ViewTimeDecline]
    // exercise: result.writeJson(outputPath)
  }
}
