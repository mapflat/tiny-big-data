package com.mapflat.exercises.demo

import com.mapflat.exercises.Serde._
import com.mapflat.exercises._
import com.mapflat.tinybigdata.{Coll, KColl}

// Read a dataset with orders. Join with the user dataset, which contains country of residence and emit the n most
// popular items in Sweden. Some usernames have been capitalised by accident, and must be converted to lower case.

object TopSwedishItemsJob {
  def main(args: Array[String]): Unit = {
    val topCount = args(1).toInt
    val orderPath = args(2)
    val userPath = args(3)
    val outputPath = args(4)

    val order = Coll.readJson[Order](orderPath)
    val user = Coll.readJson[User](userPath)

    // solution_begin

    val swedishUsers: KColl[String, User] = user
      .log("Users")
      .filter(_.country == "SE")
      .log("Swedish users")
      .groupBy(_.id)
      .log("Grouped users")

    val cleanedOrder = order
      .log("Order")
      .map(o => o.copy(user = o.user.toLowerCase))
      .log("Cleaned")

    val swedishOrder: Coll[Order] = cleanedOrder.groupBy(_.user)
      .log("Cleaned orders")
      .join(swedishUsers)
      .log("Joined")
      .values
      .log("Joined values")
      .map(_._1)
      .log("Swedish orders")

    val orderCount: Coll[ItemCount] = swedishOrder
      .groupBy(_.item)
      .log("Swedish orders by item")
      .count
      .log("Counted orders")
      .map(tup => ItemCount(tup._1, tup._2))
      .log("ItemCount")

    val result: Coll[ItemCount] = orderCount
      .groupAll
      .log("Counts grouped")
      .sortBy(-_.count)
      .log("Counts sorted")
      .take(topCount)
      .log("Top items")
      .values
      .log("Result")

    // solution_end
    // exercise: val result: Coll[ItemCount] = Coll.empty[ItemCount]
    result.writeJson(outputPath)
  }
}
