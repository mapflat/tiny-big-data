package com.mapflat.tinybigdata

import better.files._

import play.api.libs.json._

class Coll[T](private val contents: List[T]) {
  def collect(): List[T] = contents

  def cross[U](right: Coll[U]): Coll[(T, U)] = Coll.from(for {
    l <- contents
    r <- right.contents
  } yield (l, r))

  def filter(pred: T => Boolean): Coll[T] = new Coll[T](contents.filter(pred))

  def flatMap[U](f: T => Iterable[U]): Coll[U] = new Coll[U](collect().flatMap(f))

  def foreach(f: T => Unit): Coll[T] = {
    collect().foreach(f)
    this
  }

  def groupAll: KColl[Unit, T] = groupBy(_ => ())

  def groupBy[K](keyFn: T => K): KColl[K, T] = {
    val keyVals = collect().map(v => (keyFn(v), v))
    val keys = keyVals.map(_._1).distinct
    val groups = keys.map(k => (k, keyVals.filter(_._1 == k).map(_._2)))
    KColl.from(groups.toMap)
  }

  def log(label: String, printer: String => Unit = System.err.println): Coll[T] = {
    foreach(t => printer(label + ": " + t.toString))
    printer("")
    this
  }

  def map[U](mapFn: T => U): Coll[U] = new Coll(collect().map(mapFn))

  def union(other: Coll[T]): Coll[T] = new Coll(collect() ++ other.collect())

  override def toString: String = contents.map(_.toString).mkString("[\n  ", ",\n  ", "\n]")

  def writeJson(outputFile: String)(implicit toJson: Writes[T]): Unit =
    writeJson(File(outputFile))

  def writeJson(file: File)(implicit toJson: Writes[T]): Unit = {
    file.createDirectories()
    val lines = collect().map(item => Json.stringify(Json.toJson(item)))
    (file / "part-00001.json").write(lines.mkString("", "\n", "\n"))
  }
}

object Coll {
  def empty[T](): Coll[T] = new Coll[T](List.empty[T])

  def from[T](contents: Iterable[T]): Coll[T] = new Coll[T](contents.toList)

  def readJson[T](path: String)(implicit fromJson: Reads[T]): Coll[T] = readJson(File(path))

  def readJson[T](file: File)(implicit fromJson: Reads[T]): Coll[T] = {
    if (file.isDirectory)
      file.glob("*.json").map(f => readJson[T](f)).reduce((a, b) => a.union(b))
    else
      new Coll(file.lines
        .filter(!_.trim.isEmpty)
        .map(line => {
          Json.fromJson[T](Json.parse(line))
        })
        .collect({ case JsSuccess(t, _) => t })
        .toList)
  }
}
