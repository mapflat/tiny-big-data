# Mapflat batch processing exercises

Programming exercises intended to be used in hands-on data engineering course sessions.

## Getting started

### Learning a tiny bit of Scala

The exercises are written in Scala. Many are not familiar with Scala, but it is used in the course
for a good reason. You only need to know a small part of the language, with some basic features,
however. If you are not familiar with Scala, go through the first part of one of the tutorials in
the list below before the course. You will need to pick up the following topics:

* Basic syntax - classes, variables, constants, methods, traits.
* Case classes
* Function syntax - regular function/method definitions, lambda syntax, underscore parameters.
* Basic pattern matching
* Tuples
* Basic collections, List, Seq, Map
* Basic functional operations on collections: map, flatMap, filter, reduce, fold
* Option

Online Scala tutorials, in no particular order:
* [http://twitter.github.io/scala_school/](http://twitter.github.io/scala_school/)
* [https://www.tutorialspoint.com/scala/index.htm](https://www.tutorialspoint.com/scala/index.htm)
* [https://docs.scala-lang.org/tutorials/](https://docs.scala-lang.org/tutorials/)
* [http://allaboutscala.com/](http://allaboutscala.com/)
* [https://www.scala-exercises.org/scala_tutorial/terms_and_types (interactive)](https://www.scala-exercises.org/scala_tutorial/terms_and_types (interactive))
* [https://madusudanan.com/tags/#Scala](https://madusudanan.com/tags/#Scala)
* [http://danielwestheide.com/scala/neophytes.html (for intermediate Scala programmers, not beginners) ](http://danielwestheide.com/scala/neophytes.html (for intermediate Scala programmers, not beginners) )

List of tutorials: [https://hackr.io/tutorials/learn-scala](https://hackr.io/tutorials/learn-scala)

### System prerequisites

Download and install the following components:

* [JDK, at least version 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
* [Git, a recent version](https://git-scm.com/downloads).
* [IntelliJ IDEA Community edition, a recent
  version](https://www.jetbrains.com/idea/download/#section=linux). Or another development
  environment of your choice.
* IntelliJ Scala plugin. [Installed from within IntelliJ
  IDEA](https://www.jetbrains.com/help/idea/installing-updating-and-uninstalling-repository-plugins.html).
* If you did not install IntelliJ, install [Scala, at least version
  2.12](https://www.scala-lang.org/download/). Select the SBT variant.

The prerequisites above are sufficient to run the exercises on your local laptop. In case there is
technical trouble, you can run the exercises inside a Docker container. In order to prepare, please
execute these steps>

* Download a recent version of Docker.
  - If you are running Windows 10, a recent Mac OS, or Linux, you can download [native Docker community
    edition](https://www.docker.com/community-edition).
  - If you are running Windows 7 or older Mac OS, you need to download the [Docker
    Toolbox](https://docs.docker.com/toolbox/overview/). In this case, run the Docker quick start
    shell and run all commands inside that shell.
* In order to prepare for the exercise, run `docker pull ubuntu:16.04` on a good network in advance.

### Getting started

* Use git to clone this project to your laptop.
* Start IntelliJ.
* Choose `Open` and select the directory where you cloned the project. Accept the default values in
  the dialog. If you have not yet set up an SDK in IntelliJ, you may need to inform IntelliJ where
  to find the JDK that you downloaded.

You should now be able to edit the code. Each exercise is an unfinished batch processing program
under src/main/scala/com/mapflat/exercises. Your task is to finish these programs. The comments in
the program file explain the expected behaviour of the program. All programs read one or more
datasets, represented as JSON files, and emit one dataset, also JSON.

Each program has a corresponding test suite, with minimal test cases covering the happy path
behaviour, i.e. the primary input scenarios, but not unusual inputs or error handling.  The exercise
test cases are marked as ignored. In order to run a test case, replace `ignore("test name")` with
`test("test name")`. You are done with a task when all test cases run and pass.

### Running the batch processing programs

The batch processing programs are executed by running the corresponding test suites. There are three
different ways to run the tests, described below. If one does not work for you, try another one.

#### Running tests in IntelliJ

Open the project view on the left side. Expand the directory `src`. Select the directory `src/test`,
right click and select `Run ScalaTests in test`. This will run the full test suite. When working
with a single task, you can right click on the corresponding test file and run it in the same
manner. Note that you should use ScalaTests and not Unittests.

This is the most convenient way to run tests, and also supports interactive debugging.

#### Running tests with Gradle on your laptop

Run `./gradlew test`. I found no convenient way to run single test files. Let me know if you find
one that works.

Use IntelliJ or your favourite editor to edit the source files.

#### Running tests with Gradle in Docker

Run `./docker_gradle.sh`. Patiently wait for things to download. When you finally get a shell
prompt, run `./gradlew test`. It will be slow the first time but quickly in subsequent runs. Don't
exit the shell in order to keep testing quick.

Windows users cannot directly run `./gradlew test` due to line termination issues. Instead, they should
run `fromdos win_docker_test.sh` once in the docker container, and then run `./win_docker_test.sh`.

Use IntelliJ or your favourite editor to edit the source files. Note that you edit the source files
on your laptop, but run tests inside the container. The source directory is mounted into the Docker container.

#### Troubleshooting builds and tests

Avoid mixing the strategies above, since things might get confused. Incantations to try if things
get messed up:

* `./gradlew clean`
* IntelliJ: Build -> Rebuild Project.
* `rm -rf out`


## Solving the exercises

Pick one of the exercise jobs that you wish to solve. Navigate to the corresponding test and run the
test (right click on the test class name and select Run 'SomeJobTest'. The test cases are ignored, so
it will pass. Replace `ignore("test name")` with `test("test name")`, run again, and they will
fail. Your task is to implement data processing logic in SomeJob until all tests pass.

You should implement the processing logic between the `readJson` and `writeJson` calls.

I suggest starting with WashJob, then DeduplicateJob, then the other simple exercises.

### Tips and tricks

* It is easier to understand the code if you specify types for values (variables). Go to a value,
  press Alt+Enter and choose "Add type annotation to value definition". If you change the
  definition, you will get red wiggles. Go back to the value, press Alt+Enter and "Regenerate type
  annotation to value definition".
  
* The functions you write and pass to the framework, i.e. the arguments to map, filter, reduce, etc,
  operate on one or two elements at a time. You should not access any parallel collection variables,
  i.e. anything of type Coll or KColl, from these functions. You can access constant values, such as
  command line arguments.

* When you use join, cross, or groupBy, you will get tuple values. Functions that process tuple
  values need to take tuples as arguments, e.g.
  ```def myFun(tup: (String, Long))```
  
* Members of tuples are accessed with _1, _2, etc. E.g. 
  ```val name = tup._1```

* You can also unpack tuples: 
  ```val (name, count) = tup```

* Tuples can be nested: 
  ```def myOtherFun(tup: ((String, String), Long)```. 
  They are accessed as with _1, _2, etc:
  ```val country = tup._1._2```
  or by unpacking: 
  ```val ((url, country), count) = tup```

* If tuples become complex, code becomes more readable if you introduce intermediate case
  classes. Use e.g. 
  ```def tupConvert(tup: ((String, String), Long)) = UrlCountryCount(tup._1._1, tup._1._2, tup._2)```
  and then
  ```.map(tupConvert)``` 

* A KColl\[K, T\] can be viewed as a Coll\[(K, T)\] if you want to use the key values. You can just use
  the ordinary Coll methods on the KColl, since KColl inherits from Coll.

* Case classes are immutable. You cannot change a field. In order to do so, you will need to return
  a new instance where all fields but one are copied from the original. Case classes provide a
  convenience routine `copy`, e.g. 
  ```urlCountryCount.copy(country = urlCountryCount.country.toUpperCase)```

* When writing functions, you can choose between three different syntax variants:
  - Traditional function syntax: 
  ```def getName(user: User) = user.name```
  - Lambda syntax: 
  ```{ user => user.name }```
  - Implicit lambda syntax: 
  ```_.name```

* In order to view the contents during execution, add
  ```.log("Some label")```. 
  Use different labels to tell the different log points apart.

* You can run a single test by right clicking on the test name and selecting "Run SomeJobTest.test
  name".

* You can solve the exercises by calling collect() and operate on the List returned with traditional
  methods. In a cluster, that List will materialise on the master node, however. Hence, its contents
  must fit in memory. In some exercises you will need to do so, but you are only supposed to do that
  for data that you know is small, e.g. the top N items.
  
* Don't care about the performance of your solution. What goes on under the hood in a cluster
  environment is different from what happens with the in-memory implementation in Tiny Big Data. A
  cluster implementation might do something different than you would expect, so avoid premature
  optimisation.
  
* `foreach` will not be useful for the exercises. It has side effects instead of returning a
  transformed collection. It is useful e.g. for exporting results by posting to an external service.
  
### Solving exercises with Spark

There is one test, DeduplicateSparkJob, which is implemented with Spark instead of Tiny Big Data. If
you compare it with the DeduplicateJob, you will see the steps required to run the jobs with Spark
instead. The processing DSL is similar, but some details differ. The Spark DSL is also richer, and
contains more primitives.

You can choose to solve the exercises with Spark if you wish. Look at the difference between
DeduplicateJob and DeduplicateSparkJob and apply them to other jobs. Also look at the differences of
the corresponding tests and apply them to the test for the exercise that you wish to solve.


## Tiny Big Data

Trivial, in-memory implementation of functional data processing primitives, built for educational
purposes. The Tiny Big Data (TBD) DSL resembles the DSLs of Spark, Flink, Scalding/Cascading, and
Scio/Beam. It is simpler in implementation, however, and therefore easier to understand.

The core abstractions of TBD are Coll, which is an unordered dataset, and KColl, which is a
keyed/grouped dataset. Coll has one type parameter, the record type of the dataset (T), whereas
KColl has two type parameters, the key type and the record type (K, T). If you are unfamiliar with
data processing DSLs but familiar with Scala or Java collections, you can think of Coll\[T\] as a
Set\[T\], and of KColl\[K, T\] as a Map\[K, Set\[T\]\].

Coll datasets can be constructed from JSON line files on disk through 
```Coll.read[T](path)``` 
or from memory with 
```Coll.from(sequenceOfT)```.
JSON line files are expected to contain one JSON document on each line.

### Unordered dataset operations

* `collect()` Return an ordinary collection (List\[T\]) with the elements.
* `cross(c)` Return a cross product with all elements combined with all elements in c.
* `filter(p)` Apply a predicate `p` to every element. Return the items where p evaluates to true.
* `flatMap(f)` Apply a function `f` to every element. Return the concatenation of the collections
  returned from `f`.
* `foreach(f)` Apply a function `f` (which has side effects) to every element. Return the original
  dataset.
* `groupAll()` Return a keyed dataset with a single group.
* `groupBy(f)` Apply a keying function `f` to every element. Return a keyed dataset that where elements
  for which `f` returns a particular key are grouped together.
* `log(s)` Print string s and then the contents of the dataset at this point in execution. Useful
  for debugging.
* `map(f)` Apply a function `f` to every element. Return a dataset of elements returned from `f`.
* `toString` Return a human readable string describing the dataset contents.
* `union(c)` Concatenate this dataset with c.
* `writeJson(p)` Write the dataset as a JSON line file to the path `p`.

* `Coll[T].empty()` Return an empty dataset.
* `Coll[T].from(c)` Create a new dataset from a collection.
* `Coll[T].readJson(p)` Read a JSON line file from path `p`.


### Keyed dataset operations

* `count()` Count the number of records for each key. Return a keyed dataset (KColl\[K, Long\]) of
  key and counts.
* `fold(zero)(agg)` Aggregate the records in each group by passing them to an aggregation function
  `agg`. The `agg` is first sent a initial aggregation provided by the function `zero`. Each invocation
  of `agg` receives two arguments: the aggregation so far and the next record to be processed.
* `foldWithKey(zero)(agg)` Same as `fold`, but the key is passed as argument to the function `zero`.
* `reduce(agg)` Symmetrical aggregation of elements. Pass each record to the aggregation function
  `agg`, which takes two records and returns a record of the same type.
* `join(right)` Perform an inner join with the dataset `right`, which is keyed on the same key
  type. For each key, return tuples of the combination of all records in the left dataset with all
  records in the right. Records with keys only present in one dataset are discarded. Return a keyed
  dataset with tuples of the left and right value types.
* `leftJoin(right)` Perform a left join with the dataset `right`. The results are the same as for
  inner join, but for keys that are missing in the right dataset, the records in the left dataset
  are replaced with None. Return a keyed dataset with tuples of the left value type and `Option` of
  the right value type.
* `sortBy(keyFn)` Sort each group in ascending order by a particular key, extracted by `keyFn`.
* `take(n)` Take only n values from each group and discard the rest. Primarily useful after sorting
  each group.
* `values()` Discard the key and return an unordered dataset with the values. If you want to keep
  the keys, just use the keyed dataset as an unordered datset.

All of the unordered dataset operations are also available for the keyed datasets. A KColl\[K, T\]
is also a Coll\[(K, T)\].


 
