#! /usr/bin/env python3
import re
import subprocess
import tempfile
from pathlib import Path

import sys


def run(cmd, **kwargs):
    print(" ".join(cmd))
    subprocess.check_call(cmd, **kwargs)


def clean_job_source(src_dir):
    for src in src_dir.glob("*/*.scala"):
        remove_solution(src)


def remove_solution(src_file: Path):
    print("Removing solution in {}".format(src_file))
    contents = src_file.read_text()
    test_file = Path(str(src_file).replace("/main/", "/test/").replace(".scala", "Test.scala"))
    test_contents = test_file.read_text()
    example_match = re.search(r'\n *test\("Example"\) *{\n(.*?)\n *assert\(', test_contents, flags=re.DOTALL)
    if example_match:
        example = example_match.group(1)
        example_text = ("\n\n// Example input and output (copied from {}):\n".format(str(test_file.name)) +
                        "/*\n" + example + "\n*/\n\n")
    else:
        example_text = ""
    without_solution = re.sub(
        r"// *solution_begin.*?// *solution_end",
        "\n\n    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:\n" +
        "/*\n" +
        "    def myMapFn(record: SomeType): SomeType = { ... }\n"
        "\n" +
        "    val tmp1 = input_dataset\n" +
        "      .map(myMapFn)\n" +
        '      .log("tmp1")\n' +
        '\n' +
        '    val result = tmp1\n' +
        '      .filter(_.country == "SE")\n' +
        '      .log("result")\n'
        '*/\n\n',
        contents, flags=re.DOTALL)
    with_example = re.sub(r'\nobject ', example_text + '\nobject ', without_solution)
    new_contents = re.sub("// *exercise: *", "", with_example)
    src_file.write_text(new_contents)


def ignore_tests(test_dir):
    for test_src in test_dir.glob("*/*.scala"):
        ignore_tests_in(test_src)


def ignore_tests_in(test_file: Path):
    print("Ignoring tests in {}".format(test_file))
    contents = test_file.read_text()
    ignored = contents.replace('test("', 'ignore("')
    new_contents = ignored.replace('ignore("Empty input")', 'test("Empty input")')
    test_file.write_text(new_contents)


def main(argv):
    if argv[1] == "-n":
        dry = True
        repo_name = argv[2]
    else:
        dry = False
        repo_name = argv[1]
    tmp_dir = tempfile.mkdtemp(prefix="mapflat_exercise_")
    print("Cloning {} in {}".format(repo_name, tmp_dir))
    run(["git", "clone", "git@bitbucket.org:mapflat/{}".format(repo_name)], cwd=tmp_dir)
    repo = Path(tmp_dir) / repo_name
    if not dry:
        run(["git", "checkout", "-b", "solutions"], cwd=str(repo))
        run(["git", "push", "--set-upstream", "origin", "solutions"], cwd=str(repo))
        run(["git", "checkout", "master"], cwd=str(repo))

    clean_job_source(repo / "src/main/scala/com/mapflat/exercises")
    ignore_tests(repo / "src/test/scala/com/mapflat/exercises")
    run(["./gradlew", "test"], cwd=str(repo))
    if not dry:
        run(["git", "add", "."], cwd=str(repo))
        run(["git", "commit", "-m", "Remove solutions, ignore tests."], cwd=str(repo))
        run(["git", "push"], cwd=str(repo))


if __name__ == '__main__':
    main(sys.argv)
